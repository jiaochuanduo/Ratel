from distutils.core import setup

setup(
    name='Ratel',
    version='1.0',
    packages=['ratel', 'ratel.main', 'ratel.task', 'ratel.common', 'ratel.collect'],
    author='xw', requires=['selenium']
)
