# @Time    : 2017/11/3 上午10:18
# @Author  : JCD
# @File    : task_basic.py
import queue


class Task:
    pass


class Dispose:
    pass


class Execute:
    __class__ = 'Execute'

    def __init__(self):
        # 任务队列
        self.__task_queue = queue.Queue()
        # 完成队列
        self.__success_queue = queue.Queue()

    @property
    def poll_task(self):
        return self.__task_queue.get_nowait()

    def put_task(self, task):
        self.__task_queue.put_nowait(task)

    def task_done(self):
        self.__task_queue.task_done()

    @property
    def task_empty(self):
        return self.__task_queue.empty()

    @property
    def poll_success(self):
        return self.__success_queue.get_nowait()

    def put_success(self, task):
        self.__success_queue.put_nowait(task)

    def success_done(self):
        self.__success_queue.task_done()

    @property
    def success_empty(self):
        return self.__success_queue.empty()

    def get_task(self):
        pass

    def do_task(self):
        pass

    def up_task(self):
        pass

    def end(self):
        pass

    def dispose(self, result):
        pass