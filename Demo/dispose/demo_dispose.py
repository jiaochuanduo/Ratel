# @Time    : 2017/11/15 下午6:27
# @Author  : JCD
# @File    : demo_dispose.py
# 导入Dispose基类
from ratel.task.task_basic import Dispose


class RequestDispose(Dispose):
    def __init__(self):
        self.__result = None

    @property
    def get_result(self):
        return self.__result

    def set_result(self, result):
        self.__result = result

    def __str__(self):
        return '<RequestDispose> result: %s' % self.__result


class SeleniumDispose(Dispose):
    def __init__(self):
        self.__result = None

    @property
    def get_result(self):
        return self.__result

    def set_result(self, result):
        self.__result = result

    def __str__(self):
        return '<SeleniumDispose> result: %s' % self.__result

