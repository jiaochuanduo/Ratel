# @Time    : 2017/11/15 下午4:09
# @Author  : JCD
# @File    : demo_task.py
# 导入Task基类
from ratel.task.task_basic import Task


class RequestTask(Task):
    def __init__(self):
        """
        request方式的任务
        """
        self.__url = None
        self.__result = None

    @property
    def get_url(self):
        return self.__url

    def set_url(self, url):
        self.__url = url

    @property
    def get_result(self):
        return self.__result

    def set_result(self, result):
        self.__result = result


class SeleniumTask(Task):
    def __init__(self):
        """
        selenium方式的任务
        """
        self.__url = None
        self.__result = None

    @property
    def get_url(self):
        return self.__url

    def set_url(self, url):
        self.__url = url

    @property
    def get_result(self):
        return self.__result

    def set_result(self, result):
        self.__result = result
